/*
    This file is part of libhpcelo

    libhpcelo is free software: you can redistribute it and/or modify
    it under the terms of the GNU Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    libhpcelo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Public License for more details.

    You should have received a copy of the GNU Public License
    along with libhpcelo. If not, see <http://www.gnu.org/licenses/>.
*/
#include "hpcelo.h"

int main (int argc, char **argv)
{
  double t1, t2;
  double **A, **B;
  HPCELO_DECLARE_TIMER;

  int size = 10;
  if (argc > 1){
    size = atoi(argv[1]);
  }

  A = hpcelo_create_matrix (size);
  B = hpcelo_create_matrix (size);

  HPCELO_START_TIMER;

  // your parallel code here
 
  HPCELO_END_TIMER;
  HPCELO_REPORT_TIMER;
  
  hpcelo_free_matrix (A, size);
  hpcelo_free_matrix (B, size);
  return 0;
}

