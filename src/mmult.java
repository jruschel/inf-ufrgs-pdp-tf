import java.util.Random;

public class mmult
{
	static final boolean COMPARE_RESULTS = false;

	static double matrix_A[];
	static double matrix_B[];
	static double result_matrix[];
	static double result_sequential[];

	static int size;
	static int n_processes;
	static long time;	
	
	public static void main(String[] args) 
	{
		
		size = Integer.parseInt(args[0]);
		n_processes = Integer.parseInt(args[1]);
		
		matrix_A = new double[size*size];
		matrix_B = new double[size*size];
		result_matrix = new double[size*size];
		result_sequential = new double[size*size];
		
		Random rand = new Random();
		for (int i = 0; i < size; i++) 
		{
			for (int j = 0; j < size; j++) 
			{
				matrix_A[i*size+j] = Math.floor(rand.nextDouble()*100);
				matrix_B[i*size+j] = Math.floor(rand.nextDouble()*100);
				result_matrix[i*size+j] = 0;
				result_sequential[i*size+j] = 0;
			}
		}
		
		if(n_processes == 1)
		{
			mmult_sequential();
		}
		else
		{
			if (COMPARE_RESULTS)
				mmult_sequential();
			mmult_parallel();
		}
		
		System.out.println(time/ 1000000000.0);
		
		if (COMPARE_RESULTS && n_processes > 1)
			testing_results();
	}
	
	public static void mmult_sequential()
	{
		long start_time = System.nanoTime();
		for (int i = 0; i < size; i++) //Matrix_A Row
		{
			for (int j = 0; j < size; j++) //Matrix_B Column
			{ 
				for (int k = 0; k < size; k++) //Matrix_A Column
				{ 
					result_sequential[i*size+j] += matrix_A[i*size+k] * matrix_B[k*size+j];
				}
			}
		}

		long endTime = System.nanoTime();
		time = (endTime - start_time); 
	}
	
	public static void mmult_parallel()
	{
		
		Thread[] threads = new Thread[n_processes];
		int first_index = 0;
		int num_of_iterations = (int)Math.floor((size)/n_processes);
		
		long start_time = System.nanoTime();
		
		for (int i = 0; i < n_processes; i++) 
		{
			if(i == (n_processes - 1))
			{
				num_of_iterations = size - first_index;
			}
			threads[i] = new Thread(new mmult_thread(matrix_A,matrix_B,first_index,num_of_iterations, size));
			threads[i].start();
			first_index+=num_of_iterations;
		}

		try 
		{
			for (int i = 0; i < n_processes; i++) 
			{
				threads[i].join();
			}

		} catch (InterruptedException e) 
		{
			// Error - make a really big time
			time = Integer.MAX_VALUE;
			return;
		}
		long endTime = System.nanoTime();
		time = (endTime - start_time); 
	}

	public static void testing_results()
	{
		for (int i = 0; i < size; i++) 
		{
			for (int j = 0; j < size; j++) 
			{
				if(result_matrix[i*size+j] != result_sequential[i*size+j])
				{
					System.out.println("ERROR!");
					return;
				}
			}
		}
		System.out.println("SAFE!");
	}

}

class mmult_thread extends Thread
{
	double m_a[];
	double m_b[];
	int start;
	int n_calculations;
	int size;
	
	public mmult_thread(double m_a[], double m_b[], int start, int n_calculations, int size)
	{
		this.m_a = m_a;
		this.m_b = m_b;
		this.start = start;
		this.n_calculations = n_calculations;
		this.size = size;
	}

	public void run() 
	{
		for (int i = start; i <(start + n_calculations); i++) //Matrix_A Row
		{ 		
			for (int j = 0; j < size; j++) //Matrix_B Column
			{ 
				for (int k = 0; k < size; k++) //Matrix_A Column
				{ 
					mmult.result_matrix[i*size+j] += m_a[i*size+k] * m_b[k*size+j];
				}
			}
		}
	}
}

