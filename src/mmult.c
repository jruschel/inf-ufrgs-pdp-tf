/*
  Matrix Multiplication
    by João Paulo T Ruschel - jptruschel@inf.ufrgs.br
	
  Multiplies two square matrices of parameterizable sizes using different
    algorithms, parallel and non-parallel.
*/

/* Args: size threads */

#include "hpcelo.h"
#include <string.h>
#include <omp.h>

#define DEFAULT_MATRIX_SIZE 64
#define DEFAULT_CHUNK_SIZE 1
#define FALSE 0
#define TRUE 1
#define COMPARE_RESULTS FALSE

/* Compare two contiguous 2D matrices. */
int compare_2dc_matrix(double *A, double *B, int size);

/* Calculates the optimal chunk size on the current machine */
int get_chunk_n(int m_size);
int thread_count_set;

/* Calculates C = A * B using an iterative (non-parallel algorithm).
  Measures time using HPCELO directives. */
void mmult_iterative(double *A, double *B, double *C, int size) {
  HPCELO_DECLARE_TIMER;
  int i, j, k;

  HPCELO_START_TIMER;
  for (i = 0; i < size; i++) {
    for (j = 0; j < size; j++) {
      C[i*size+j] = (double) 0;
      for (k = 0; k < size; k++) {
        C[i*size+j] += A[i*size+k] * B[k*size+j];
      }
    }
  }
  HPCELO_END_TIMER;
  printf("%f", HPCELO_GET_TIMER);
}

/* Calculates C = A * B using a parallel algorithm.
  Measures time using HPCELO directives. */
void mmult_parallel(double *A, double *B, double *C, int size) {
  HPCELO_DECLARE_TIMER;
  int i, j, k;

  HPCELO_START_TIMER;
  #pragma omp parallel for schedule(static, get_chunk_n(size)) private (i,j,k) shared (A,B,C,size)
  for (i = 0; i < size; i++) {
    for (j = 0; j < size; j++) {
      double sum = 0;
      for (k = 0; k < size; k++) {
        sum += A[i*size+k] * B[k*size+j];
      }
      C[i*size+j] = sum;
    }
  }
  HPCELO_END_TIMER;
  printf("%f", HPCELO_GET_TIMER);
}

/* MAIN */
int main (int argc, char **argv) {
  double *A, *B, *C, *D;
  int size = DEFAULT_MATRIX_SIZE;
  thread_count_set = 0;

  if (argc < 3) {
    printf("Wrong usage! Use <size> <threads>\n");
    return 0;
  }

  // set size based on input
  size = atoi(argv[1]);

  // set number of threads to use
  thread_count_set = atoi(argv[2]);

  // intialize all arrays
  A = hpcelo_create_matrix (size);
  B = hpcelo_create_matrix (size);
  C = malloc((long)size*(long)size*sizeof(double*));
  D = malloc((long)size*(long)size*sizeof(double*));

  // If 1 thread, execute iterative
  if (thread_count_set == 1) {
    mmult_iterative(A, B, C, size);
    if (COMPARE_RESULTS) {
      mmult_iterative(A, B, D, size);
      printf(",%d", compare_2dc_matrix(C, D, size));
    }
  } 
  // Else, execute parallel
  else {
    mmult_parallel(A, B, C, size);
    if (COMPARE_RESULTS) {
      mmult_iterative(A, B, D, size);
      printf(",%d", compare_2dc_matrix(C, D, size));
    }
  }

  printf("\n"); //new line
  
  // free all matrices
  hpcelo_free_matrix (A);
  hpcelo_free_matrix (B);
  hpcelo_free_matrix (C);
  hpcelo_free_matrix (D);

  return 0;
}

/* Compare two contiguous 2D matrices. */
int compare_2dc_matrix(double *A, double *B, int size) {
  long i=0;
  long size2 = (long)size * (long)size;
  while (i < size2) {
    if (A[i] != B[i])
      return FALSE;
    i++;
  }
  return TRUE;
}

/* Calculates the optimal chunk size on the current machine */
int get_chunk_n(int m_size) {
  int chunk = 0;
  if (m_size > thread_count_set && thread_count_set > 0){
    chunk = m_size/thread_count_set;
  }
  else {
    chunk = DEFAULT_CHUNK_SIZE;
  }
  return chunk;
}
