import java.io.*;
import java.util.*;

public class mandelbrot 
{
	static final boolean COMPARE_RESULTS = false;
	static final boolean SAVE_TO_FILE = false;
	static final int DEFAULT_MATRIX_SIZE = 64;
	static final int DEFAULT_CHUNK_SIZE = 1;
	
	/* color component ( R or G or B) is coded from 0 to 255 */
	/* it is 24 bit color RGB file */
	static int MaxColorComponentValue=255;
	static int matrix[];
	static int size;
	static int n_processes;
	static long time;	
	
	/* MAIN */
	public static void main(String[] args) 
	{
		// set size based on input
		size = Integer.parseInt(args[0]);

		// set number of threads to use
		n_processes = Integer.parseInt(args[1]);

		matrix = new int[size*size*3];
		
		if(n_processes == 1)
		{
			mandelbrot_sequential();
		}
		else
		{
			if (COMPARE_RESULTS) {
				mandelbrot_sequential();
				try
				{
					writeImage(matrix, "mandel-sequential.ppm");			
				}catch (IOException ex) { System.err.println(ex); }
			}
			mandelbrot_parallel();
		}
		
		if (SAVE_TO_FILE || COMPARE_RESULTS) 
		{
			try
			{
				writeImage(matrix, "mandel.ppm");			
			}catch (IOException ex) { System.err.println(ex); } 
		}
		 
		 System.out.println(time/ 1000000000.0);
	}
	
	/* Calculates Mandelbrot set using an iterative (non-parallel algorithm).
	Measures time using System.nanoTime() */
	public static void mandelbrot_sequential()
	{
		int i, j, k;

		/* screen ( integer) coordinate */
		int iX,iY;
		int iXmax = size; 
		int iYmax = size;
		/* world ( double) coordinate = parameter plane*/
		double Cx,Cy;
		double CxMin=-1.5;
		double CxMax=0.5;
		double CyMin=-1.0;
		double CyMax=1.0;
		/* */
		double PixelWidth=(CxMax-CxMin)/iXmax;
		double PixelHeight=(CyMax-CyMin)/iYmax;
		/* Z=Zx+Zy*i  ;   Z0 = 0 */
		double Zx, Zy;
		double Zx2, Zy2; /* Zx2=Zx*Zx;  Zy2=Zy*Zy  */
		double sqZs2; /* sqZs2 = sqrt(Zx2 + Zy2); */
		/* Iteration */
		int Iteration;
		int IterationMax=200;
		/* bail-out value , radius of circle ;  */
		double EscapeRadius=2;
		double ER2=EscapeRadius*EscapeRadius;
	
		// start counting execution time
		long start_time = System.nanoTime();
		
		/* compute and write image data bytes to output array */
		for(iY=0;iY<iYmax;iY++) 
		{
			Cy=CyMin + iY*PixelHeight;
			if (Math.abs(Cy)< PixelHeight/2) 
				Cy=0.0; /* Main antenna */
			for(iX=0;iX<iXmax;iX++) 
			{
				Cx=CxMin + iX*PixelWidth;
				/* initial value of orbit = critical point Z= 0 */
				Zx=0.0;
				Zy=0.0;
				Zx2=Zx*Zx;
				Zy2=Zy*Zy;
				/* */
				for (Iteration=0;Iteration<IterationMax && ((Zx2+Zy2)<ER2);Iteration++) 
				{
					Zy=2*Zx*Zy + Cy;
					Zx=Zx2-Zy2 + Cx;
					Zx2=Zx*Zx;
					Zy2=Zy*Zy;
				};
				/* compute  pixel color (24 bit = 3 bytes) */
				if (Iteration==IterationMax)
				{ 
					/*  interior of Mandelbrot set = black */
					matrix[(iY*size+iX)*3] = 0;
					matrix[(iY*size+iX)*3+1] = 0;
					matrix[(iY*size+iX)*3+2] = 0;                    
				}
				else 
				{ 
					/* exterior of Mandelbrot set = white */
					sqZs2 = Math.sqrt(Zx2 + Zy2);
					int brightness = (int) (256 * log2(1.75 + Iteration - log2(log2(sqZs2))) / log2((double)IterationMax));

					matrix[(iY*size+iX)*3] = brightness;
					matrix[(iY*size+iX)*3+1] = brightness;
					matrix[(iY*size+iX)*3+2] = 255;
				};
			}
		}
		
		long end_time = System.nanoTime();
		time = (end_time - start_time);
	}
	
	/* Calculates Mandelbrot set using an parallel algorithm.
	Measures time using System.nanoTime() */
	public static void mandelbrot_parallel()
	{
		Thread[] threads = new Thread[n_processes];
		int first_index = 0;
		int num_of_iterations = (int)Math.floor((size)/n_processes);
		
		// start counting execution time
		long start_time = System.nanoTime();
		
		for (int i = 0; i < n_processes; i++) 
		{
			if(i == (n_processes - 1))
			{
				num_of_iterations = size - first_index;
			}
			threads[i] = new Thread(new mandelbrot_thread(size, first_index, num_of_iterations));
			threads[i].start();
			first_index+=num_of_iterations;
		}
		
		try 
		{
			for (int i = 0; i < n_processes; i++) 
			{
				threads[i].join();
			}

		} catch (InterruptedException e) 
		{
			// Error - make a really big time
			time = Integer.MAX_VALUE;
			return;
		}
		
		long end_time = System.nanoTime();
		time = (end_time - start_time);
	}
	
	/*
		writeImage - write to a ppm image from array

	*/
	public static void writeImage(int color[], String fName) throws IOException
	{
		OutputStream out = null;
		String testString = "P6\n # \n "+size+" \n "+size+" \n "+MaxColorComponentValue+" \n";
		try{
			out = new FileOutputStream(fName);
			byte[] totalBytes = testString.getBytes();
			out.write(totalBytes);
			out.write(totalBytes);
			
			byte[] r,g,b;
			
			for(int i=0; i < size*size; i++) 
			{
				//r = color[i*3].getBytes();
				out.write((char)color[i*3]);
				out.write((char)color[i*3+1]);
				out.write((char)color[i*3+2]);
			}
		}
		finally{
			if(out !=null)
				out.close();
		}	
	}
	
	
	public static double log2(double num)
	{
		return (Math.log(num)/Math.log(2));
	}

}

class mandelbrot_thread extends Thread
{
	int i, j, k;
	/* screen ( integer) coordinate */
	int iX,iY;
	
	/* world ( double) coordinate = parameter plane*/
	double Cx,Cy;
	double CxMin=-1.5;
	double CxMax=0.5;
	double CyMin=-1.0;
	double CyMax=1.0;
	/* */
	
	/* Z=Zx+Zy*i  ;   Z0 = 0 */
	double Zx, Zy;
	double Zx2, Zy2; /* Zx2=Zx*Zx;  Zy2=Zy*Zy  */
	double sqZs2; /* sqZs2 = sqrt(Zx2 + Zy2); */
	/* Iteration */
	int Iteration;
	int IterationMax=200;
	/* bail-out value , radius of circle ;  */
	double EscapeRadius=2;
	double ER2=EscapeRadius*EscapeRadius;
	
	int size;
	int start;
	int n_calculations;
		
	public mandelbrot_thread(int size, int start, int n_calculations)
	{
		this.size = size;
		this.start = start;
		this.n_calculations = n_calculations;
	}

	public void run() 
	{
		int iXmax = size; 
		int iYmax = size;
		
		double PixelWidth=(CxMax-CxMin)/iXmax;
		double PixelHeight=(CyMax-CyMin)/iYmax;
		
		/* compute and write image data bytes to output array */
		for(iY=start;iY<(start + n_calculations);iY++) 
		{
			Cy=CyMin + iY*PixelHeight;
			if (Math.abs(Cy)< PixelHeight/2) 
				Cy=0.0; /* Main antenna */
			for(iX=0;iX<iXmax;iX++) 
			{
				Cx=CxMin + iX*PixelWidth;
				/* initial value of orbit = critical point Z= 0 */
				Zx=0.0;
				Zy=0.0;
				Zx2=Zx*Zx;
				Zy2=Zy*Zy;
				/* */
				for (Iteration=0;Iteration<IterationMax && ((Zx2+Zy2)<ER2);Iteration++) 
				{
					Zy=2*Zx*Zy + Cy;
					Zx=Zx2-Zy2 + Cx;
					Zx2=Zx*Zx;
					Zy2=Zy*Zy;
				};
				/* compute  pixel color (24 bit = 3 bytes) */
				if (Iteration==IterationMax)
				{ 
					/*  interior of Mandelbrot set = black */
					mandelbrot.matrix[(iY*size+iX)*3] = 0;
					mandelbrot.matrix[(iY*size+iX)*3+1] = 0;
					mandelbrot.matrix[(iY*size+iX)*3+2] = 0;                    
				}
				else 
				{ 
					/* exterior of Mandelbrot set = white */
					sqZs2 = Math.sqrt(Zx2 + Zy2);
					int brightness = (int) (256 * log2(1.75 + Iteration - log2(log2(sqZs2))) / log2((double)IterationMax));

					mandelbrot.matrix[(iY*size+iX)*3] = brightness;
					mandelbrot.matrix[(iY*size+iX)*3+1] = brightness;
					mandelbrot.matrix[(iY*size+iX)*3+2] = 255;
				};
			}
		}
	}
	
	public static double log2(double num)
	{
		return (Math.log(num)/Math.log(2));
	}
}
