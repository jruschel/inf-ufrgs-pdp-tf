/*
  Mandelbrot
    by João Paulo T Ruschel - jptruschel@inf.ufrgs.br

  Calculates Mandelbrot set for a specific discretion size.
    Outputs to a image file.
*/

/* Args: size threads */

#include "hpcelo.h"
#include <string.h>
#include <omp.h>
#include <math.h>

#define DEFAULT_MATRIX_SIZE 64
#define DEFAULT_CHUNK_SIZE 1
#define FALSE 0
#define TRUE 1
#define COMPARE_RESULTS FALSE
#define SAVE_TO_FILE FALSE

/* Compare two contiguous 2D matrices. */
int compare_2dc_matrix(int *A, int *B, int size);

/* Calculates the optimal chunk size on the current machine */
int get_chunk_n(int m_size);
int thread_count_set;

/* color component ( R or G or B) is coded from 0 to 255 */
/* it is 24 bit color RGB file */
const int MaxColorComponentValue=255;

/* Calculates Mandelbrot set using an iterative (non-parallel algorithm).
  Values are output to *out.
  Measures time using HPCELO directives. */
void mandelbrot_iterative(int *out, int size) {
  HPCELO_DECLARE_TIMER;
  int i, j, k;

  /* screen ( integer) coordinate */
  int iX,iY;
  int iXmax = size; 
  int iYmax = size;
  /* world ( double) coordinate = parameter plane*/
  double Cx,Cy;
  const double CxMin=-1.5;
  const double CxMax=0.5;
  const double CyMin=-1.0;
  const double CyMax=1.0;
  /* */
  double PixelWidth=(CxMax-CxMin)/iXmax;
  double PixelHeight=(CyMax-CyMin)/iYmax;
  /* Z=Zx+Zy*i  ;   Z0 = 0 */
  double Zx, Zy;
  double Zx2, Zy2; /* Zx2=Zx*Zx;  Zy2=Zy*Zy  */
  double sqZs2; /* sqZs2 = sqrt(Zx2 + Zy2); */
  /* Iteration */
  int Iteration;
  const int IterationMax=200;
  /* bail-out value , radius of circle ;  */
  const double EscapeRadius=2;
  double ER2=EscapeRadius*EscapeRadius;

  HPCELO_START_TIMER;
  /* compute and write image data bytes to output array */
  for(iY=0;iY<iYmax;iY++) {
    Cy=CyMin + iY*PixelHeight;
    if (fabs(Cy)< PixelHeight/2) Cy=0.0; /* Main antenna */
    for(iX=0;iX<iXmax;iX++) {
      Cx=CxMin + iX*PixelWidth;
      /* initial value of orbit = critical point Z= 0 */
      Zx=0.0;
      Zy=0.0;
      Zx2=Zx*Zx;
      Zy2=Zy*Zy;
      /* */
      for (Iteration=0;Iteration<IterationMax && ((Zx2+Zy2)<ER2);Iteration++) 
      {
        Zy=2*Zx*Zy + Cy;
        Zx=Zx2-Zy2 + Cx;
        Zx2=Zx*Zx;
        Zy2=Zy*Zy;
        };
        /* compute  pixel color (24 bit = 3 bytes) */
        if (Iteration==IterationMax)
        { /*  interior of Mandelbrot set = black */
          out[(iY*size+iX)*3] = 0;
          out[(iY*size+iX)*3+1] = 0;
          out[(iY*size+iX)*3+2] = 0;                    
        }
        else 
        { /* exterior of Mandelbrot set = white */
          sqZs2 = sqrt(Zx2 + Zy2);
          int brightness = 256 * log2(1.75 + Iteration - log2(log2(sqZs2))) / log2((double)IterationMax);

          out[(iY*size+iX)*3] = brightness;
          out[(iY*size+iX)*3+1] = brightness;
          out[(iY*size+iX)*3+2] = 255;
      };
    }
  }

  HPCELO_END_TIMER;
  printf("%f", HPCELO_GET_TIMER);
}

/* Calculates Mandelbrot set using an parallel algorithm.
  Values are output to *out.
  Measures time using HPCELO directives. */
void mandelbrot_parallel(int *out, int size) {
  HPCELO_DECLARE_TIMER;
  
  /* screen ( integer) coordinate */
  int iX,iY;
  int iXmax = size; 
  int iYmax = size;
  /* world ( double) coordinate = parameter plane*/
  double Cx,Cy;
  const double CxMin=-1.5;
  const double CxMax=0.5;
  const double CyMin=-1.0;
  const double CyMax=1.0;
  /* */
  double PixelWidth=(CxMax-CxMin)/iXmax;
  double PixelHeight=(CyMax-CyMin)/iYmax;
  /* Z=Zx+Zy*i  ;   Z0 = 0 */
  double Zx, Zy;
  double Zx2, Zy2; /* Zx2=Zx*Zx;  Zy2=Zy*Zy  */
  double sqZs2; /* sqZs2 = sqrt(Zx2 + Zy2); */
  /* Iteration */
  int Iteration;
  const int IterationMax=200;
  /* bail-out value , radius of circle ;  */
  const double EscapeRadius=2;
  double ER2=EscapeRadius*EscapeRadius;

  HPCELO_START_TIMER;
  /* compute and write image data bytes to output array */
  #pragma omp parallel for schedule(dynamic) num_threads(thread_count_set) shared(out, size, iXmax, iYmax, PixelWidth, PixelHeight, ER2) private(iX, iY, Cx, Cy, Zx, Zy, Zx2, Zy2, Iteration)
  for(iY=0;iY<iYmax;iY++) {
    Cy=CyMin + iY*PixelHeight;
    if (fabs(Cy)< PixelHeight/2) Cy=0.0; /* Main antenna */
    for(iX=0;iX<iXmax;iX++) {
      Cx=CxMin + iX*PixelWidth;
      /* initial value of orbit = critical point Z= 0 */
      Zx=0.0;
      Zy=0.0;
      Zx2=Zx*Zx;
      Zy2=Zy*Zy;
      /* */
      for (Iteration=0;Iteration<IterationMax && ((Zx2+Zy2)<ER2);Iteration++) 
      {
        Zy=2*Zx*Zy + Cy;
        Zx=Zx2-Zy2 + Cx;
        Zx2=Zx*Zx;
        Zy2=Zy*Zy;
        };
        /* compute  pixel color (24 bit = 3 bytes) */
        if (Iteration==IterationMax)
        { /*  interior of Mandelbrot set = black */
          out[(iY*size+iX)*3] = 0;
          out[(iY*size+iX)*3+1] = 0;
          out[(iY*size+iX)*3+2] = 0;                    
        }
        else 
        { /* exterior of Mandelbrot set = white */
          sqZs2 = sqrt(Zx2 + Zy2);
          int brightness = 256 * log2(1.75 + Iteration - log2(log2(sqZs2))) / log2((double)IterationMax);

          out[(iY*size+iX)*3] = brightness;
          out[(iY*size+iX)*3+1] = brightness;
          out[(iY*size+iX)*3+2] = 255;
      };
    }
  }

  HPCELO_END_TIMER;
  printf("%f", HPCELO_GET_TIMER);
}

/* MAIN */
int main (int argc, char **argv) {
  int *A, *B;
  int size = DEFAULT_MATRIX_SIZE;
  thread_count_set = 0;

  if (argc < 3) {
    printf("Wrong usage! Use <size> <threads>\n");
    return 0;
  }

  // set size based on input
  size = atoi(argv[1]);

  // set number of threads to use
  thread_count_set = atoi(argv[2]);

  // intialize all arrays
  A = malloc(size*size*3*sizeof(int));
  B = malloc(size*size*3*sizeof(int));
  unsigned int si = (unsigned int) (((size*size*3*sizeof(int))/1024)/1024);

  // If 1 thread, execute iterative
  if (thread_count_set == 1) {
    mandelbrot_iterative(A, size);
    if (COMPARE_RESULTS) {
      mandelbrot_iterative(B, size);
      printf(",%d", compare_2dc_matrix(A, B, size));
    }
  } 
  // Else, execute parallel
  else {
    mandelbrot_parallel(A, size);
    if (COMPARE_RESULTS) {
      mandelbrot_iterative(B, size);
      printf(",%d", compare_2dc_matrix(A, B, size));
    }
  }

  printf("\n"); //new line

  /* write image to file */
  if (SAVE_TO_FILE) {
    FILE *fp;
    char filename[50];
    sprintf(filename, "mandel_%d.ppm", size);
    //="mandel.ppm";
    char *comment="# ";/* comment should start with # */
    
    /*create new file,give it a name and open it in binary mode  */
    fp= fopen(filename,"wb"); /* b -  binary mode */
    /*write ASCII header to the file*/
    fprintf(fp,"P6\n %s\n %d\n %d\n %d\n",comment,size,size,MaxColorComponentValue);

    int i;
    static unsigned char color[3];
    for(i=0; i < size*size; i++) {
      color[0] = A[i*3];
      color[1] = A[i*3+1];
      color[2] = A[i*3+2];
      fwrite(color,1,3,fp);
    }
    fclose(fp);
  }
  
  // free all matrices
  free (A);
  free (B);

  return 0;
}

/* Compare two contiguous 2D matrices. */
int compare_2dc_matrix(int *A, int *B, int size) {
  long i=0;
  long size2 = (long)size * (long)size;
  while (i < size2) {
    if (A[i] != B[i])
      return FALSE;
    i++;
  }
  return TRUE;
}

/* Calculates the optimal chunk size on the current machine */
int get_chunk_n(int m_size) {
  int chunk = 0;
  if (m_size > thread_count_set && thread_count_set > 0){
    chunk = m_size/thread_count_set;
  }
  else {
    chunk = DEFAULT_CHUNK_SIZE;
  }
  return chunk;
}
