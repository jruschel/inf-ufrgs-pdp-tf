#chmod u+x <>
DESIGN="mm_design.csv"
BDIR="../bin"
CNAME="mmult"
LOG="mm_log.txt"

echo "Starting Java Mmult Tests At: $(date)" 

sed 1d $DESIGN | while IFS=, read name runNOin runNO runNOstd size threads time plataform
do
    echo "Now running #"${name//[\"]/}":" ${size//[\"]/}"," ${threads//[\"]/}
    printf "j,"${size//[\"]/}","${threads//[\"]/}"," >> $LOG
    java -cp $BDIR $CNAME ${size//[\"]/} ${threads//[\"]/} >> $LOG
done 

echo "Done Java Mmult Tests At: $(date)"
