#chmod u+x <>
DESIGN="ma_design.csv"
BDIR="../bin"
CNAME="mandelbrot"
LOG="ma_log.txt"

echo "Starting Java Mandelbrot Tests At: $(date)" 

sed 1d $DESIGN | while IFS=, read name runNOin runNO runNOstd size threads time plataform
do
    echo "Now running #"${name//[\"]/}":" ${size//[\"]/}"," ${threads//[\"]/}
    printf "j,"${size//[\"]/}","${threads//[\"]/}"," >> $LOG
    java -Xmx6g -cp $BDIR $CNAME ${size//[\"]/} ${threads//[\"]/} >> $LOG
done 

echo "Done Java Mandelbrot Tests At: $(date)"
