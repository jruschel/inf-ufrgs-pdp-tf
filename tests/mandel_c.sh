#chmod u+x <>
DESIGN="ma_design.csv"
BIN="../bin/mandelbrot.o "
LOG="ma_log.txt"

echo "Starting C Mandelbrot Tests At: $(date)" 

sed 1d $DESIGN | while IFS=, read name runNOin runNO runNOstd size threads time plataform
do
    echo "Now running #"${name//[\"]/}":" ${size//[\"]/}"," ${threads//[\"]/}
    printf "c,"${size//[\"]/}","${threads//[\"]/}"," >> $LOG
    $BIN ${size//[\"]/} ${threads//[\"]/} >> $LOG
done 

echo "Done C Mandelbrot Tests At: $(date)"
