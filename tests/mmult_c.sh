#chmod u+x <>
DESIGN="mm_design.csv"
BIN="../bin/mmult.o "
LOG="mm_log.txt"

echo "Starting C MMult Tests At: $(date)" 

sed 1d $DESIGN | while IFS=, read name runNOin runNO runNOstd size threads time plataform
do
    echo "Now running #"${name//[\"]/}":" ${size//[\"]/}"," ${threads//[\"]/}
    printf "c,"${size//[\"]/}","${threads//[\"]/}"," >> $LOG
    $BIN ${size//[\"]/} ${threads//[\"]/} >> $LOG
done 

echo "Done C Mmult Tests At: $(date)"
