ODIR=bin
SDIR=src
IDIR=lib

OMPFLAGS=-fopenmp
CFLAGS=-I$(IDIR) -O3

_OBJ = hpcelo.o
OBJ = $(patsubst %,$(ODIR)/%,$(_OBJ))

all: mmult_c mandelbrot_c mmult_java mandelbrot_java

mmult_c: $(OBJ)
	gcc -o $(ODIR)/mmult.o $(SDIR)/mmult.c $(OBJ) $(OMPFLAGS) $(CFLAGS) -lm

mandelbrot_c: $(OBJ)
	gcc -o $(ODIR)/mandelbrot.o $(SDIR)/mandelbrot.c $(OBJ) $(OMPFLAGS) $(CFLAGS) -lm

mmult_java: $(OBJ)
	javac -d $(ODIR) $(SDIR)/mmult.java

mandelbrot_java: $(OBJ)
	javac -d $(ODIR) $(SDIR)/mandelbrot.java

$(ODIR)/hpcelo.o: $(IDIR)/hpcelo.c
	gcc -o $(ODIR)/hpcelo.o -c $(IDIR)/hpcelo.c

.PHONY: clean

clean:
	rm -f $(ODIR)/*.o
	rm -f $(ODIR)/*.class
